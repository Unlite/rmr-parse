<?php

namespace app\services;

use Exception;
use GdImage;

class ImageDownloadService
{
    protected string $url;
    protected array $vars;
    protected string $saveFolder;
    protected bool $addWatermark;
    protected GdImage $stamp;

    public function __construct(string $url, array $vars, string $saveFolder, bool $addWatermark)
    {
        $this->url = $url;
        $this->vars = $vars;
        $this->saveFolder = $saveFolder;
        $this->addWatermark = $addWatermark;
        $this->stamp = imagecreatefromjpeg('watermark.jpg');
    }

    public function downloadImageByArticle(string $filePath, string $article): ?string
    {
        if (!$this->fileExists($article)) {
            if ($this->UrlExists($filePath)) {
                $savePath = "{$this->saveFolder}{$article}.{$this->getFileExtensionByUrl($filePath)}";
                copy($filePath, $savePath);
                if ($this->addWatermark) {
                    $this->addWatermark($savePath);
                }
            } else {
                return null;
            }
        }

        return $this->saveFolder . $this->findFile($article);
    }

    protected function UrlExists(string $url): bool
    {
        return str_contains(get_headers($url)[0], "200 OK");
    }

    public function fileExists(string $article): bool
    {
        return !empty($this->findFile($article));
    }

    public function findFile(string $article)
    {
        $files = array_diff(scandir($this->saveFolder), array('..', '.'));
        foreach ($files as $file) {
            if (str_contains($file, $article)) {
                return $file;
            }
        }
        return null;
    }

    protected function getFileExtensionByUrl(string $filePath): string
    {
        $array = explode('.', $filePath);
        return end($array);
    }

    protected function createImageUrl(string $character, string $number)
    {
        $replace = [$character, $character . $number];
        return str_replace($this->vars, $replace, $this->url);
    }

    protected function splitArticle(string $article)
    {
        return [$article[0], substr($article, 1)];
    }

    public function addWatermark($path)
    {
        $mimeType = mime_content_type($path);

        switch ($mimeType) {
            case 'image/png':
                $im = imagecreatefrompng($path);
                break;
            case 'image/jpg':
            case 'image/jpeg':
                $im = imagecreatefromjpeg($path);
                break;
            default:
                throw new Exception("Unsupported image mime format {$mimeType}");
        }


        $sy = imagesy($this->stamp);

        imagecopy($im, $this->stamp, 0, imagesy($im) - $sy, 0, 0, imagesx($this->stamp), imagesy($this->stamp));

        switch ($mimeType) {
            case 'image/png':
                $im = imagecreatefrompng($path);
                imagepng($im, $path);
                break;
            case 'image/jpg':
            case 'image/jpeg':
                imagejpeg($im, $path);
                break;
            default:
                throw new Exception("Unsupported image mime format {$mimeType}");
        }

        imagedestroy($im);
    }
}