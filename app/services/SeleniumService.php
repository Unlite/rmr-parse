<?php

namespace app\services;

use app\Exception\ArticleNotFoundException;
use Facebook\WebDriver\Chrome\ChromeDriver;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\ScriptTimeoutException;
use Facebook\WebDriver\Exception\TimeoutException;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;

class SeleniumService
{
    private $driver;

    public function __construct(
        private readonly string $seleniumUrl,
        private readonly string $url,
    ) {
    }

    public function getDriver()
    {
        if (!$this->driver) {
            $options = new ChromeOptions();
            $options->setBinary(INDEX_DIR . '\GoogleChromePortable64\App\Chrome-bin\chrome.exe');

            // Create $capabilities and add configuration from ChromeOptions
            $capabilities = DesiredCapabilities::chrome();
            $capabilities->setCapability(ChromeOptions::CAPABILITY, $options);

            $this->driver = RemoteWebDriver::create($this->seleniumUrl, $capabilities);
            $this->driver->manage()->window()->minimize();
        }
        return $this->driver;
    }

    public function clearDriver()
    {
        if ($this->driver) {
            $this->driver = null;
        }
    }

    /**
     * @throws NoSuchElementException
     * @throws TimeoutException
     * @throws ArticleNotFoundException
     */
    public function findUrlByArticle(string $article)
    {
        $this->getDriver()->get($this->url);

        $searchBar = $this->getDriver()->findElement(
            WebDriverBy::cssSelector('.bigsearch .search input[name="search"]')
        );

        $searchBar->click();

        $searchBar->sendKeys($article);

        try {
            $this->getDriver()->wait(3)->until(
                function () {
                    $element = $this->getDriver()->findElement(
                        WebDriverBy::cssSelector('.bigsearch .search > .dropdown-menu')
                    );
                    return $element->isDisplayed();
                },
                'Unable wait until searchbar appear'
            );

            $listItems = $this->getDriver()->wait(3)->until(
                function () {
                    return $this->getDriver()->findElements(
                        WebDriverBy::cssSelector('.bigsearch .search > .dropdown-menu li a')
                    );
                },
                'Unable wait until searchbar appear'
            );
        } catch (ScriptTimeoutException $exception) {
            throw new ArticleNotFoundException('Article not found in mrm search list');
        }

        if (!$listItems) {
            throw new ArticleNotFoundException('Article not found in mrm search list');
        }

        $listItem = $listItems[0];

        $productUrl = $listItem->getAttribute('href');

        $this->getDriver()->close();

        $this->clearDriver();

        return $productUrl;
    }
}