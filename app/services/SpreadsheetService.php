<?php

namespace app\services;

use Iterator;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SpreadsheetService
{
    private Spreadsheet $spreadsheet;

    public function __construct(
        private readonly SpreadsheetConfig $config
    ) {
        $this->spreadsheet = IOFactory::load($this->config->fileName);
    }

    /**
     * @return Worksheet[]
     */
    public function getSheets(): Iterator
    {
        return $this->spreadsheet->getWorksheetIterator();
    }

    public function getSpreadsheet(): Spreadsheet
    {
        return $this->spreadsheet;
    }

}