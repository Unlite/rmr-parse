<?php

namespace app\services;

use app\Exception\ArticleNotFoundException;
use DOMDocument;
use DOMXpath;
use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Exception\ScriptTimeoutException;

class HtmlParserService
{

    public function __construct(
        private readonly SeleniumService $seleniumService,
        private readonly string $path
    ) {
    }

    public function getXpathByArticle($article): DOMXpath
    {
        $htmlPath = $this->getArticleHtmlFile($article);

        return $this->createHtmlXpath($htmlPath);
    }

    public function getProductTitleByArticle($article): ?string
    {
        $xpath = $this->getXpathByArticle($article);

        $rowsData = $xpath->query(
            '//ul[contains(@class, "breadcrumb")]//h1[contains(@class, "inbreadcrumb")]'
        );
        if ($rowsData->item(0)) {
            return $rowsData->item(0)->textContent;
        } else {
            return null;
        }
    }

    public function getProductCodeByArticle(string $article): ?string
    {
        $xpath = $this->getXpathByArticle($article);

        $rowsData = $xpath->query(
            '//div[contains(@id, "tab-description")]'
        );
        if ($item = $rowsData->item(0)) {
            $array = explode(',', $item->nodeValue);
            if (preg_match('/200+[\d\s]+$/', trim(end($array)), $matches, PREG_OFFSET_CAPTURE)) {
                return trim(end($array));
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function getImageUrlByArticle(string $article): ?string
    {
        $xpath = $this->getXpathByArticle($article);

        $rowsData = $xpath->query('//img[contains(@id, "mainImage")]');

        if ($item = $rowsData->item(0)) {
            return $item->getAttribute('src');
        } else {
            return null;
        }
    }

    protected function getHtmlPath($article): string
    {
        return $this->path . $article . '.html';
    }

    protected function getArticleHtmlFile($article): string
    {
        $htmlPath = $this->getHtmlPath($article);
        if (!file_exists($htmlPath)) {
            $productUrl = $this->seleniumService->findUrlByArticle($article);
            $htmlPath = $this->downloadHtml($productUrl, $article);
            sleep(2);
        }
        return $htmlPath;
    }

    public function articleExists($article): bool
    {
        $htmlPath = $this->getHtmlPath($article);
        if (!file_exists($htmlPath)) {
            try {
                $productUrl = $this->seleniumService->findUrlByArticle($article);
            } catch (ArticleNotFoundException $articleNotFoundException) {
                return false;
            } catch (ScriptTimeoutException $timeoutException) {
                return false;
            } catch (NoSuchElementException $noSuchElementException) {
                return false;
            }
            if ($productUrl) {
                echo "Article ".EchoService::addSpaces($article)." found, caching.\n";
                $this->downloadHtml($productUrl, $article);
                sleep(2);
                return true;
            } else {
                return false;
            }
        } else {
            echo "Article ".EchoService::addSpaces($article)." found in cache.\n";
            return true;
        }
    }

    protected function createHtmlXpath(string $htmlPath): DOMXpath
    {
        $code = file_get_contents($htmlPath);
        $doc = new DOMDocument();
        @$doc->loadHTML($code);
        return new DOMXpath($doc);
    }

    protected function downloadHtml(string $downloadHtml, string $article): string
    {
        $savePath = $this->getHtmlPath($article);
        copy($downloadHtml, $savePath);
        return $savePath;
    }

}