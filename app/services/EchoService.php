<?php

namespace app\services;

class EchoService
{
    protected const MAX_LONG = 16;
    public static function addSpaces(?string $article): string
    {
        $toAdd = self::MAX_LONG - ($article ? strlen($article) : 0);
        if($toAdd > 0){
            return $article.str_repeat(' ',$toAdd)."\t";
        }
        return $article;
    }
}