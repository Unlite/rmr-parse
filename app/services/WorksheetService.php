<?php

namespace app\services;

use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Worksheet\ColumnDimension;
use PhpOffice\PhpSpreadsheet\Worksheet\ColumnIterator;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Helper\Dimension as CssDimension;

class WorksheetService
{
    public function __construct(
        private readonly Worksheet $worksheet,
        private readonly SpreadsheetConfig $config,
    ) {
    }

    public function getArticleColumns(): ColumnIterator
    {
        return $this->worksheet->getColumnIterator(
            $this->config->articleColumn,
            $this->config->articleColumn,
        );
    }

    public function getImageColumnDimension(): ColumnDimension
    {
        return $this->worksheet->getColumnDimension($this->config->imageColumn);
    }

    public function getImageCell(Cell $cell): Cell
    {
        return $this->worksheet->getCell($this->config->imageColumn . $cell->getRow());
    }

    public function getTitleCell(Cell $cell): Cell
    {
        return $this->worksheet->getCell($this->config->titleColumn . $cell->getRow());
    }

    public function getCodeCell(Cell $cell): Cell
    {
        return $this->worksheet->getCell($this->config->codeColumn . $cell->getRow());
    }

    public function fillImage(
        Cell $cell,
        ColumnDimension $imageColumnDimension,
        string $imagePath,
    ): bool {
        $imageCell = $this->getImageCell($cell);
        if (!$this->cellContainsImage($imageCell)) {
            $rowDimension = $this->worksheet->getRowDimension($cell->getRow());
            $this->drawImage(
                $imagePath,
                $rowDimension->getRowHeight(CssDimension::UOM_PIXELS),
                $imageColumnDimension->getWidth(CssDimension::UOM_PIXELS),
                $imageCell->getCoordinate()
            );
            return true;
        }
        return false;
    }

    public function fillTitle(Cell $cell, string $title): bool
    {
        $titleCell = $this->getTitleCell($cell);
        if (empty($titleCell->getValue()) && $title) {
            $titleCell->setValue($title);
            return true;
        }

        return false;
    }

    public function cellHasTitle(Cell $cell):bool
    {
        $titleCell = $this->getTitleCell($cell);
        return !empty($titleCell->getValue());
    }

    public function fillCode(Cell $cell, string $code): bool
    {
        $codeCell = $this->getCodeCell($cell);

        if (empty($codeCell->getValue()) && $code) {
            $codeCell->setValue($code);
            return true;
        }

        return false;
    }

    public function cellHasCode(Cell $cell):bool
    {
        $codeCell = $this->getCodeCell($cell);
        return !empty($codeCell->getValue());
    }

    public function cellContainsImage(Cell $cell): bool
    {
        $imageCell = $this->getImageCell($cell);
        foreach ($this->worksheet->getDrawingCollection() as $drawing) {
            if ($drawing->getCoordinates() == $imageCell->getCoordinate()) {
                return true;
            }
        }
        return false;
    }

    public function drawImage(
        string $path,
        string $height,
        string $width,
        string $coordinates
    ): Drawing {
        $drawing = new Drawing();
        $drawing->setPath($path);
        $drawing->setOffsetX(5);
        $drawing->setOffsetX2(5);
        $drawing->setOffsetY(5);
        $drawing->setOffsetY2(5);

        $drawing->setResizeProportional(false);
        $drawing->setHeight($height - 10);
        $drawing->setWidth($width - 10);
        $drawing->setCoordinates($coordinates);
        $drawing->setWorksheet($this->worksheet);
        return $drawing;
    }
}