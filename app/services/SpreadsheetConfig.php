<?php

namespace app\services;

class SpreadsheetConfig
{
    public function __construct(
        public readonly string $fileName,
        public readonly string $articleColumn,
        public readonly string $titleColumn,
        public readonly string $imageColumn,
        public readonly string $codeColumn,
    )
    {
    }

    public static function create(array $config): SpreadsheetConfig
    {
        return new self(
            fileName: $config['name'],
            articleColumn: $config['article']['column'],
            titleColumn: $config['title']['column'],
            imageColumn: $config['image']['column'],
            codeColumn: $config['code']['column'],
        );
    }
}