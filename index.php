<?php

const INDEX_DIR = __DIR__;

spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

require 'vendor/autoload.php';

use app\services\EchoService;
use app\services\HtmlParserService;
use app\services\ImageDownloadService;
use app\services\SeleniumService;
use app\services\SpreadsheetConfig;
use app\services\SpreadsheetService;
use app\services\WorksheetService;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$config = require 'config.php';

$imageDownloadService = new ImageDownloadService(
    $config['image']['url'],
    $config['image']['vars'],
    $config['image']['saveFolder'],
    $config['image']['addWatermark']
);

$seleniumService = new SeleniumService(
    $config['parser']['seleniumUrl'],
    $config['parser']['baseUrl'],
);

$htmlParseService = new HtmlParserService(
    $seleniumService,
    $config['parser']['saveFolder']
);

$spreadSheetService = new SpreadsheetService(
    SpreadsheetConfig::create($config['sheet'])
);

/** Load $inputFileName to a Spreadsheet Object  **/

foreach ($spreadSheetService->getSheets() as $sheet) {
    $worksheetService = new WorksheetService($sheet, SpreadsheetConfig::create($config['sheet']));
    $imageColumnDimension = $worksheetService->getImageColumnDimension();

    foreach ($worksheetService->getArticleColumns() as $column) {
        foreach ($column->getCellIterator() as $cell) {
            $article = $cell->getValue();

            if (!isArticle($article)) {
                echo "Article " . EchoService::addSpaces($article) . " is not valid. skipping...\n";
                continue;
            }


            if (!$htmlParseService->articleExists($article)) {
                echo "Article " . EchoService::addSpaces($article) . " not found on MRM website . skipping...\n";
                continue;
            }

            if (
                $worksheetService->cellHasTitle($cell)
                && $worksheetService->cellHasCode($cell)
                && $worksheetService->cellContainsImage($cell)
            ) {
                echo "Article " . EchoService::addSpaces($article) . " fully filled. skipping...\n";
                continue;
            }

            $inserted = [];

            $title = $htmlParseService->getProductTitleByArticle($article);

            if ($title && $worksheetService->fillTitle($cell, $title)) {
                $inserted[] = 'title';
            }

            $code = $htmlParseService->getProductCodeByArticle($article);
            if ($code && $worksheetService->fillCode($cell, $code)) {
                $inserted[] = 'code';
            }

            $imageUrl = $htmlParseService->getImageUrlByArticle($article);
            if ($imageUrl) {
                $imagePath = $imageDownloadService->downloadImageByArticle($imageUrl, $article);
                if ($imagePath) {
                    $worksheetService->fillImage($cell, $imageColumnDimension, $imagePath);
                    $inserted[] = 'image';
                } else {
                    echo "WARNING: Article " . EchoService::addSpaces($article) . " has a broken image link\n";
                }
            }

            if ($inserted) {
                $insertedStr = join(' and ', $inserted);
                echo "Inserted " . EchoService::addSpaces($article) . " data: {$insertedStr} \n";
            }
        }
    }
}


$writer = new Xlsx($spreadSheetService->getSpreadsheet());
echo "Saving...\n";
$writer->save("output.xlsx");
echo "Done...\n";

function isArticle(?string $string): bool
{
    return $string && (bool)preg_match('/^\w\d+$/m', $string);
}
