<?php
return [
    'sheet' => [
        'article' => [
            'column' => 'D',
        ],
        'title' => [
            'column' => 'C',
        ],
        'image' => [
            'column' => 'B',
        ],
        'code' => [
            'column' => 'M',
        ],
        'name' => 'test.xlsx',
    ],
    'image' => [
        'url' => 'https://mrmpower.ru/image/cache/catalog/{letter}/{article}-400x400.jpg',
        'vars' => [
            '{letter}',
            '{article}',
        ],
        'saveFolder' => 'images/',
        'addWatermark' => true
    ],
    'parser' => [
        'seleniumUrl' => 'http://localhost:4444',
        'baseUrl' => 'https://mrmpower.ru/register_account',
        'saveFolder' => 'pages/'
    ]
];